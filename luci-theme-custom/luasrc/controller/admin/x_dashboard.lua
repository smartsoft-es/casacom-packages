module("luci.controller.admin.x_dashboard", package.seeall)

function index()
    local fs = require "nixio.fs"
    local config = require "luci.config"
    if fs.basename(config.main.mediaurlbase) == 'custom' then
		local page  = node("admin")
		page.target  = template("admin_status/dashboard")
		page.sysauth_template = "sysauth_custom"
    end
end
