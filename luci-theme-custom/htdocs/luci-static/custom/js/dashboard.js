$(document).ready(function () {
	$('select').selectBox({
		mobile: true
	});


	var windowWidth = $(window).width();
	if (windowWidth < 1100) {
		$('.gridster ul li').attr({
		    'data-col' : 1,
		    'data-row' : 1
		});
	}
	var gridster;
	var log = document.getElementById('log');

	gridster = $(".gridster ul").gridster({
		widget_base_dimensions: [140, 140],
		widget_margins: [0, 0],
		resize: {
			enabled: true
		}
	}).data('gridster');
});