--[[
by Pnia
01/02/2018
]]--
module("luci.controller.admin.nodes", package.seeall)

function index()
	if nixio.fs.access("/etc/config/6lowpannat") then
		entry({"admin", "nodes"}, template("nodes"), _("gZ 6lowPAN"))
		entry({"nodes"}, call("nodes_status"), nil).leaf = true
	end
	entry({"admin", "wifinodes"}, template("wifinodes"), _("gZ WiFi"))
	entry({"wifinodes"}, call("wifinodes_status"), nil).leaf = true
end

local function create_mac_from_eui64(ipv6)
	local mac = ''
	local h1, h2, h3, h4, h5, h6 = ipv6:match("fd00::212:(%x+)(%x%x):(%x+)(%x%x):(%x+)(%x%x)")
	h1 = string.format("%02x",tonumber("0x"..h1))
	h3 = string.format("%02x",tonumber("0x"..h3))
	h5 = string.format("%02x",tonumber("0x"..h5))
	if h1 and h2 and h3 and h4 and h5 and h6 then
		mac = "00:12:"..h1..":"..h2..":"..h3..":"..h4..":"..h5..":"..h6
	end
	return mac:upper()
end

local function create_port_from_eui64(ipv6)
	local bit = require "nixio".bit
	local port = 0
	local h1, h2, h3, h4, h5, h6 = ipv6:match("fd00::212:(%x+)(%x%x):(%x+)(%x%x):(%x+)(%x%x)")
	h5 = string.format("%02x",tonumber("0x"..h5))
	if h5 and h6 then
		port = bit.bor(bit.band( tonumber("0x"..h5..h6), 0xFFF), 0xC000)
	end
	return port
end

function nodes_status()
	luci.http.prepare_content("application/json")
	local uci  = require "luci.model.uci".cursor()
	local server_ip = uci:get("tunslip", "tunslip", "ipv6addr")
	local nodes = { }
	local swc = io.popen("wget -O- http://[" .. server_ip .. "]", "r")
	if swc then
		local json = swc:read("*a")
		for ip, via in json:gmatch("\"dst\":\"([%w:]+)/128\",\"via\":\"([%w:]+)\"") do
			nodes[#nodes+1] = {
				ip	= ip,
				port	= create_port_from_eui64(ip),
				mac	= create_mac_from_eui64(ip),
				via = via
			}
		end
		swc:close()
	end
	local json = {}
	json["count"] = #nodes
	json["nodes"] = nodes
	luci.http.write_json(json)
end


function wifinodes_status()
	local stat = require "luci.tools.status"
	local hosts = luci.sys.net.host_hints();
	local nodes = {}
	--https://es.adminsub.net/mac-address-finder/espressif
	local espressif = { "EC:FA:BC","DC:4F:22","D8:A0:1D","B4:E6:2D","AC:D0:74","A4:7B:9D","A0:20:A6","90:97:D5","68:C6:3A","60:01:94","5C:CF:7F","54:5A:A6","30:AE:A4","2C:3A:E8","24:B2:DE","24:0A:C4","18:FE:34","84:F3:EB","68:C6:BA","80:7D:3A","CC:50:E3","BC:DD:C2","C8:FD:19"}
	for _, assoclist in ipairs(stat.wifi_assoclist()) do
		for _,espressifMac in pairs(espressif) do
			--Show only the Espressif Inc. MAC addresses
			if assoclist.bssid:sub(1,8)==espressifMac then
				assoclist.mac = assoclist.bssid
				if hosts[assoclist.bssid] then
					assoclist.host = hosts[assoclist.bssid].name
					assoclist.ipv4 = hosts[assoclist.bssid].ipv4
					assoclist.ipv6 = hosts[assoclist.bssid].ipv6
				end
				assoclist.ifname = assoclist.ifname
				assoclist.name = assoclist.name
				assoclist.net_quality = assoclist.quality
				nodes[#nodes+1] = assoclist
			end
		end
	end


	local json = {}
	json["count"] = #nodes
	json["nodes"] = nodes
	luci.http.prepare_content("application/json")
	luci.http.write_json(json)
end
