--[[
OpenWrt users by Pnia --
inspired by https://github.com/Hostle/openwrt-luci-multi-user --
30/12/2016 --
]]--

m = Map("users", translate("User Configuration"), translate("Add / Remove or Edit Users."))

local fs = require "nixio.fs"
local groups = {"admin", "user"}
local usw = require "luci.users"

local s, o

m.on_after_commit = function()
			usw.load_ui_user_file()
			usw.load_sys_user_file()
			usw.add_users()
			usw.del_users()			
		    end

s = m:section(TypedSection, "user")
s.anonymous = true
s.addremove = true

s:tab("user",  translate("User Setting"))

function s.parse(self, ...)
	TypedSection.parse(self, ...)
end

name = s:taboption("user", Value, "user_name", translate("User Name"))
name.rmempty = false
name.optional = false
function name.validate(self, value, section)
	if value=="root" then
		return nil, translate("Invalid user name")
	end
	return value
end

user_group = s:taboption("user", ListValue, "user_group", translate("User Group"))
for k, v in ipairs(groups) do
	user_group:value(v)
end

pw1 = s:taboption("user", Value, "pw1", translate("Password"))
pw1.password = true
pw1.write = function(self, section, value)
	local username = name:formvalue(section)
	local password = pw1:formvalue(section)
	if luci.sys.user.setpasswd(username, password) == 0 then
		s.message = translate("Unknown Error, password not changed!")
	end
end

	local admin = luci.dispatcher.node("admin")
	local childs = luci.dispatcher.node_childs(admin)			
	if #childs > 0 then
		for index, name in ipairs(childs) do
			local current = admin.nodes[name]
			local menukey = "menu_"..name
			if current then
				local currentchilds = luci.dispatcher.node_childs(current)
				if #currentchilds > 0 then
					s:tab(menukey,  current.title)
					--add options in tab
					local options = s:taboption(menukey, MultiValue, name)
					options.class = "cbi-value-list"
					for index, subname in ipairs(currentchilds) do
						if subname~="overview" then
							local current = current.nodes[subname]
							if options.default == nil then
								options.default = subname
							else
								options.default = options.default .. " " .. subname
							end						
							options:value(subname, luci.util.striptags(current.title))
						end
					end
				end
			end
		end
	end

return m
