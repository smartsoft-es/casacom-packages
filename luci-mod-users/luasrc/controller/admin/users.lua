--[[
OpenWrt users by Pnia --
inspired by https://github.com/Hostle/openwrt-luci-multi-user --
30/12/2016 --
]]--
module("luci.controller.admin.users", package.seeall)
local menu_hidden = false

function index()
	require "luci.users"
	
	node("admin").sysauth = luci.users.get_valid_users()
	node("servicectl").sysauth = luci.users.get_valid_users()
	
	local user = luci.users.get_user()
	if user == "root" or luci.users.get_current_user_group() == "admin" then
	  entry({"admin", "users"}, cbi("admin_users/users", {autoapply=true}), _("Users"), 55).index = true
	end
	if user ~= nil then
	  name = string.sub(user:upper(),0,1) .. user:sub(2,-1)
	  entry({"admin", "passwd"}, cbi("admin_users/passwd"), _("Change password"), 55).index = true
	end
	
	--hide menus first time a template is rendered
	local tpl = require("luci.template")
	tpl.render = function(name, scope)
		if not menu_hidden then
			menu_hidden = true
			require "luci.users".hide_menus()
		end
		return tpl.Template(name):render(scope or getfenv(2))
	end	
end

--## modified passwd function from system.lua, usses user to determine which users password to change ##--
function action_passwd()
	local p1 = luci.http.formvalue("pwd1")
	local p2 = luci.http.formvalue("pwd2")
	local stat = nil

	if p1 or p2 then
		if p1 == p2 then
			stat = luci.sys.user.setpasswd(user, p1)
		else
			stat = 10
		end
	end
	luci.template.render("admin_users/passwd", {stat=stat})
end