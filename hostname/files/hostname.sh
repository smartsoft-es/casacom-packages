#!/bin/sh
. /lib/functions.sh

checkOUI="__OUI__"
currentOUI=$(cat /sys/class/net/__DEVICE__/address | awk -F ':' '{print $1$2$3}' | awk '{print toupper($0)}')
if  [[ $checkOUI != 000000 ]] && [[ $checkOUI != $currentOUI ]] ;
then
	#exit if OUI doest not matches
	exit 0
fi

nullHostName="__NULL__"

SUFFIX=$(cat /sys/class/net/__DEVICE__/address | awk -F ':' '{print $5$6}' | awk '{print toupper($0)}')
hostname="__PREFIX__-$SUFFIX"

#set system hostname
currentHostName=$(uci get system.@system[0].hostname)
if [ "$currentHostName" = "$nullHostName" -o "$currentHostName" = "OpenWrt" -o "$currentHostName" = "LEDE" ]
then
        $(uci set system.@system[0].hostname=$hostname)
        $(uci commit system)
        echo "$hostname" > /proc/sys/kernel/hostname
fi

#set hostname for network interfaces with dhcp protocol
interface_hostname() {
        local section="$1"
        local currentHostName=$(uci get network.$section.hostname 2>/dev/null)
        local proto=$(uci get network.$section.proto 2>/dev/null)
        local nullHostName="$2"
        local newHostName="$3"

        if [ "$proto" = "dhcp" -a \( -z "$currentHostName" -o "$currentHostName" = "$nullHostName" -o "$currentHostName" = "OpenWrt" -o "$currentHostName" = "LEDE" \) ]
        then
                $(uci set network.$section.hostname=$newHostName)
        fi
}
config_load network
config_foreach interface_hostname interface $nullHostName $hostname
$(uci commit network)

#set the default wireless config, only if is access point, exists and has default ssid
old_ssid=$(uci get wireless.@wifi-iface[0].ssid 2>/dev/null)
old_mode=$(uci get wireless.@wifi-iface[0].mode 2>/dev/null)
if [ "$old_mode" = "ap" -a -n "$old_ssid" -a \( "$old_ssid" = "OpenWrt" -o "$old_ssid" = "LEDE" -o "$old_ssid" = "$nullHostName" \) ]
then
        uci set wireless.@wifi-iface[0].ssid=$hostname
        uci commit wireless
fi

#set the default wireless config, only if is access point, exists and has default ssid
old_ssid=$(uci get wireless.@wifi-iface[1].ssid 2>/dev/null)
old_mode=$(uci get wireless.@wifi-iface[1].mode 2>/dev/null)
if [ "$old_mode" = "ap" -a -n "$old_ssid" -a \( "$old_ssid" = "OpenWrt" -o "$old_ssid" = "LEDE" -o "$old_ssid" = "$nullHostName" \) ]
then
        uci set wireless.@wifi-iface[1].ssid=$hostname
        uci commit wireless
fi

if [ -f "/etc/uci-defaults/wifi" ]
then
	sed -i -- "s/ssid=$nullHostName/ssid=$hostname/g" /etc/uci-defaults/wifi
fi


#configure samba if exists
if [ -f "/etc/config/samba" ]
then
        uci set samba.@samba[0].name="__PREFIX__"
        uci set samba.@samba[0].description="$hostname"
        uci commit samba
fi